import React from 'react';
import AppNavigation from './src/navigation';
import firebase from '@react-native-firebase/app';
import Routes from './src/routes';

var firebaseConfig = {
  apiKey: 'AIzaSyB697qY0QXNcMUGWZApoL41vFzJTClcXEw',
  authDomain: 'endarrtrainingapp.firebaseapp.com',
  databaseURL: 'https://endarrtrainingapp.firebaseio.com',
  projectId: 'endarrtrainingapp',
  storageBucket: 'endarrtrainingapp.appspot.com',
  messagingSenderId: '548040397710',
  appId: '1:548040397710:web:bea38f354bda7b89834adf',
  measurementId: 'G-ND6CEKGX04',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <Routes />;
};

export default App;
