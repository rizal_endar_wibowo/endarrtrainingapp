import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const TodoListItem = ({todo, onPressRemove}) => {
  return (
    <View
      style={{
        elevation: 2,
        borderColor: 'black',
        borderWidth: 1,
        marginVertical: 5,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <View>
        <Text>{todo.date}</Text>
        <Text>{todo.title}</Text>
      </View>
      <TouchableOpacity onPress={onPressRemove}>
        <Icon name="delete" size={20} />
      </TouchableOpacity>
    </View>
  );
};

export default TodoListItem;
