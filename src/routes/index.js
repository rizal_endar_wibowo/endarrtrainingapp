import React, {useState} from 'react';
import {Context} from '../context';
import AppNavigation from '../navigation';

const Routes = () => {
  const [isOnboarding, setIsOnboarding] = useState(true);
  const [isLogin, setIsLogin] = useState(false);

  return (
    <Context.Provider
      value={{isOnboarding, setIsOnboarding, isLogin, setIsLogin}}>
      <AppNavigation />
    </Context.Provider>
  );
};

export default Routes;
