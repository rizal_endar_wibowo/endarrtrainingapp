import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  btnCircle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#191970',
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    flex: 1,
  },

  title: {
    fontSize: 24,
    color: '#191970',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },

  blueContainer: {
    height: '35%',
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnChangePicture: {
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
  },
  card: {
    elevation: 3,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    marginHorizontal: 20,
    marginTop: -30,
  },
});
