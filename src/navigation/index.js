import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/SplashScreen';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Profile from '../screens/Tugas2/Profile';
import Home from '../screens/Home';
import Icon from 'react-native-vector-icons/AntDesign';
import Register from '../screens/Register';
import Maps from '../screens/Maps';
import Chart from '../screens/Chart';
import Chat from '../screens/Chat';
import {Context} from '../context';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

const MainNavigation = () => {
  const state = React.useContext(Context);

  const authScreens = {
    Auth: AuthNavigation,
    Tabs: TabsScreen,
  };

  const dashboardScreen = {
    Tabs: TabsScreen,
    Auth: AuthNavigation,
  };

  const introScreen = {
    Intro: IntroStackScreen,
    Auth: AuthNavigation,
  };

  return (
    <Stack.Navigator>
      {Object.entries({
        ...(state.isOnboarding
          ? introScreen
          : state.isLogin
          ? dashboardScreen
          : authScreens),
      }).map(([name, component]) => (
        <Stack.Screen
          name={name}
          component={component}
          options={{headerShown: false}}
        />
      ))}
      <Stack.Screen
        name="Chart"
        component={Chart}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
};

const AuthNavigation = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const IntroStack = createStackNavigator();
const IntroStackScreen = () => (
  <IntroStack.Navigator headerMode="none">
    <IntroStack.Screen name="Intro" component={Intro} />
  </IntroStack.Navigator>
);

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Home" component={Home} />
  </HomeStack.Navigator>
);

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator headerMode="none">
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
);

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: '#3EC6FF',
    }}>
    <Tabs.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color}) => <Icon name="home" color={color} size={26} />,
      }}
    />
    <Tabs.Screen
      name="Maps"
      component={Maps}
      options={{
        tabBarLabel: 'Maps',
        tabBarIcon: ({color}) => (
          <Icon name="enviromento" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Chat"
      component={Chat}
      options={{
        tabBarLabel: 'Chat',
        tabBarIcon: ({color}) => <Icon name="mail" color={color} size={26} />,
      }}
    />
    <Tabs.Screen
      name="Profile"
      component={ProfileStackScreen}
      options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({color}) => <Icon name="user" color={color} size={26} />,
      }}
    />
  </Tabs.Navigator>
);

function AppNavigation() {
  const [isLoading, setIsLoading] = React.useState(true);
  const state = React.useContext(Context);
  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
      checkStatus();
    }, 3000);
    const checkStatus = async () => {
      try {
        const statusLogin = await AsyncStorage.getItem('isLogin');
        if (statusLogin != null) {
          state.setIsLogin(true);
        } else {
          state.setIsLogin(false);
        }
        const statusOnboarding = await AsyncStorage.getItem('isOnboarding');
        if (statusOnboarding != null) {
          state.setIsOnboarding(false);
        } else {
          state.setIsOnboarding(true);
        }
      } catch (error) {
        console.log(error);
      }
    };
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
}

export default AppNavigation;
