import React from 'react';
import {View, Text} from 'react-native';

const App = () => {
  return (
    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <Text>Halo Kelas React Native Lanjutan SanberCode!</Text>
    </View>
  );
};

export default App;
