import React, {useEffect} from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

const Home = ({navigation}) => {
  useEffect(() => {
    const setIsLogin = async () => {
      try {
        await AsyncStorage.setItem('isLogin', JSON.stringify(true));
        console.log('set', 'OK');
      } catch (error) {
        console.log(error);
      }
    };
    setTimeout(() => {
      setIsLogin();
    }, 500);
  }, []);
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.card}>
          <View style={styles.cardHeader}>
            <Text style={styles.headerText}>Kelas</Text>
          </View>
          <View style={styles.cardBody}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Chart')}
              style={styles.item}>
              <Icon name="react" size={50} color="white" />
              <Text style={styles.titleItem}>React Native</Text>
            </TouchableOpacity>
            <View style={styles.item}>
              <Icon name="language-python" size={50} color="white" />
              <Text style={styles.titleItem}>Data Science</Text>
            </View>
            <View style={styles.item}>
              <Icon name="react" size={50} color="white" />
              <Text style={styles.titleItem}>React JS</Text>
            </View>
            <View style={styles.item}>
              <Icon name="laravel" size={50} color="white" />
              <Text style={styles.titleItem}>Laravel</Text>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardHeader}>
            <Text style={styles.headerText}>Kelas</Text>
          </View>
          <View style={styles.cardBody}>
            <View style={styles.item}>
              <Icon name="wordpress" size={50} color="white" />
              <Text style={styles.titleItem}>Wordpress</Text>
            </View>
            <View style={styles.item}>
              <Image
                source={require('../assets/icons/website-design.png')}
                style={styles.icon}
              />
              <Text style={styles.titleItem}>Desain Grafis</Text>
            </View>
            <View style={styles.item}>
              <Icon name="server" size={50} color="white" />
              <Text style={styles.titleItem}>Web Server</Text>
            </View>
            <View style={styles.item}>
              <Image
                source={require('../assets/icons/ux.png')}
                style={styles.icon}
              />
              <Text style={styles.titleItem}>UI/UX Desain</Text>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.cardHeader}>
            <Text style={styles.headerText}>Summary</Text>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>React Native</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>20 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>Data Science</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>30 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>React JS</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>66 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>Laravel</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>60 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>Wordpress</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>20 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>Desain Grafis</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>30 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>Web Server</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>66 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.list}>
            <Text style={styles.title}>UI/UX Desain</Text>
            <View style={styles.content}>
              <Text style={styles.textContent}>Today</Text>
              <Text style={styles.textContent}>60 Orang</Text>
            </View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Total</Text>
              <Text style={styles.textContent}>100 Orang</Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  card: {
    backgroundColor: '#088dc4',
    borderRadius: 10,
    marginBottom: 10,
  },
  cardHeader: {
    padding: 5,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
  },
  headerText: {
    color: 'white',
  },
  cardBody: {
    backgroundColor: '#3EC6FF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
  },
  item: {
    alignItems: 'center',
  },
  titleItem: {
    color: 'white',
  },
  icon: {
    width: 50,
    height: 50,
    margin: 1,
  },
  list: {marginBottom: 5},
  title: {
    backgroundColor: '#3EC6FF',
    color: 'white',
    padding: 5,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 50,
  },
  textContent: {
    color: 'white',
  },
});

export default Home;
