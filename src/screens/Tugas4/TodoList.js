import React, {useContext} from 'react';
import {View, Text, TextInput, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import TodoListItem from '../../components/TodoListItem';
import {RootContext} from './index';

const TodoList = () => {
  const state = useContext(RootContext);

  const renderItem = ({item, index}) => {
    return (
      <TodoListItem todo={item} onPressRemove={() => state.removeTodo(index)} />
    );
  };

  return (
    <View style={{padding: 10}}>
      <Text>Masukan Todo List</Text>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <TextInput
          onChangeText={(input) => state.handleOnChange(input)}
          defaultValue={state.input}
          style={{
            flex: 1,
            borderColor: 'black',
            borderWidth: 1,
            paddingStart: 5,
          }}
        />
        <TouchableOpacity
          onPress={() => state.addTodo()}
          style={{
            backgroundColor: '#3EC6FF',
            justifyContent: 'center',
            padding: 20,
            marginStart: 5,
          }}>
          <Icon name="plus" size={15} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={state.todos}
        renderItem={renderItem}
        keyExtractor={(item, i) => i.toString()}
      />
    </View>
  );
};

export default TodoList;
