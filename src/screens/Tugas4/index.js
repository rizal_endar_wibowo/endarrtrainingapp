import React, {createContext, useState} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);
  const handleOnChange = (value) => {
    setInput(value);
  };
  const addTodo = () => {
    const day = new Date().getDate();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const today = `${day}/${month}/${year}`;
    setTodos([...todos, {title: input, date: today}]);
    setInput('');
  };
  const removeTodo = (i) => {
    setTodos(todos.filter((todo, index) => index !== i));
  };
  return (
    <RootContext.Provider
      value={{
        input,
        todos,
        handleOnChange,
        addTodo,
        removeTodo,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
