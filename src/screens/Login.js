import React, {useState, useEffect} from 'react';
import {Image, StatusBar, Text, View} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {styles} from '../style';
import api from '../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onLoginPress = () => {
    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        navigation.replace('Tabs');
      })
      .catch((err) => {
        alert('Login Gagal, Silahkan Coba Lagi!');
      });
  };

  // const onLoginPress = () => {
  //   let data = {
  //     email: email,
  //     password: password,
  //   };

  //   Axios.post(`${api}/login`, data, {timeout: 20000})
  //     .then((res) => {
  //       saveToken(res.data.token);
  //       navigation.replace('Tabs');
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //       alert('Login Gagal, Silahkan Coba Lagi');
  //     });
  // };

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
      console.log('Save Error', error);
    }
  };

  const configGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '548040397710-5knkq46re946khs4dmj14gh28odjh18t.apps.googleusercontent.com',
      offlineAccess: false,
    });
  };

  useEffect(() => {
    configGoogleSignIn();
  }, []);

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log(idToken);

      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.replace('Tabs');
    } catch (error) {
      console.log(error);
      alert('Login Gagal, Silahkan Coba Lagi');
    }
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        navigation.replace('Tabs');
      })
      .catch((error) => {
        alert('Authentication failed');
      });
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <Image
        source={require('../assets/image/logo.png')}
        style={[styles.image, {marginVertical: 30}]}
      />
      <View style={{flex: 1, alignSelf: 'stretch', padding: 20}}>
        <Text>Username</Text>
        <TextInput
          style={{borderBottomWidth: 1}}
          placeholder="Username/Email"
          value={email}
          onChangeText={(email) => setEmail(email)}
        />
        <Text>Password</Text>
        <TextInput
          style={{borderBottomWidth: 1}}
          placeholder="Password"
          secureTextEntry
          value={password}
          onChangeText={(password) => setPassword(password)}
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#3EC6FF',
            padding: 10,
            alignItems: 'center',
            marginTop: 20,
          }}
          onPress={() => onLoginPress()}>
          <Text style={{color: 'white'}}>LOGIN</Text>
        </TouchableOpacity>
        <Text style={{marginVertical: 10, alignSelf: 'center'}}>-OR-</Text>
        <GoogleSigninButton
          onPress={() => signInWithGoogle()}
          style={{width: '100%', height: 50}}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#191970',
            padding: 10,
            alignItems: 'center',
            marginTop: 5,
          }}
          onPress={() => signInWithFingerprint()}>
          <Text style={{color: 'white'}}>SIGN IN WITH FINGERPRINT</Text>
        </TouchableOpacity>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>Belum Punya Akun?</Text>
          <TouchableOpacity
            style={{marginStart: 5}}
            onPress={() => navigation.navigate('Register')}>
            <Text style={{color: '#191970'}}>Registrasi</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login;
