import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import TodoListItem from '../../components/TodoListItem';

const TodoList = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  const handleOnChange = (value) => {
    setInput(value);
  };

  const addTodo = () => {
    const day = new Date().getDate();
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    const today = `${day}/${month}/${year}`;
    setTodos([...todos, {title: input, date: today}]);
    setInput('');
  };

  const removeTodo = (i) => {
    setTodos(todos.filter((todo, index) => index !== i));
  };

  const renderItem = ({item, index}) => {
    return <TodoListItem todo={item} onPressRemove={() => removeTodo(index)} />;
  };

  return (
    <View style={{padding: 10}}>
      <Text>Masukan Todo List</Text>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <TextInput
          onChangeText={(input) => handleOnChange(input)}
          defaultValue={input}
          style={{
            flex: 1,
            borderColor: 'black',
            borderWidth: 1,
            paddingStart: 5,
          }}
        />
        <TouchableOpacity
          onPress={() => addTodo()}
          style={{
            backgroundColor: '#3EC6FF',
            justifyContent: 'center',
            padding: 20,
            marginStart: 5,
          }}>
          <Icon name="plus" size={15} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={todos}
        renderItem={renderItem}
        keyExtractor={(item, i) => i.toString()}
      />
    </View>
  );
};

export default TodoList;
