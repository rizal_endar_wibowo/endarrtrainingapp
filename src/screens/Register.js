import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  Modal,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {styles} from '../style';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const option = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(option);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => alert('Upload Success'))
      .catch((error) => alert(error));
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={(ref) => {
              camera = ref;
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                margin: 10,
                borderRadius: 35,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => toggleCamera()}>
                <MaterialCommunity name="rotate-3d-variant" size={20} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '40%',
                width: 200,
                borderRadius: 100,
                borderColor: 'black',
                borderWidth: 2,
                alignSelf: 'center',
              }}
            />
            <View
              style={{
                height: '20%',
                width: 200,
                borderColor: 'black',
                borderWidth: 2,
                alignSelf: 'center',
                marginVertical: 20,
              }}
            />
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingBottom: 40,
              }}>
              <TouchableOpacity
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 35,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => takePicture()}>
                <MaterialCommunity name="camera" size={40} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View style={styles.blueContainer}>
        <TouchableOpacity
          style={{alignItems: 'center'}}
          onPress={() => setIsVisible(true)}>
          <Image
            source={
              photo === null
                ? require('../assets/image/profile_pic.jpg')
                : {uri: photo.uri}
            }
            style={{height: 100, width: 100, borderRadius: 50}}
          />
          <Text style={styles.btnChangePicture}>Change picture</Text>
        </TouchableOpacity>
      </View>
      {renderCamera()}
      <View style={styles.card}>
        <Text>Nama</Text>
        <TextInput style={{borderBottomWidth: 1}} placeholder="Nama" />
        <Text>Email</Text>
        <TextInput style={{borderBottomWidth: 1}} placeholder="Email" />
        <Text>Password</Text>
        <TextInput
          style={{borderBottomWidth: 1}}
          placeholder="Password"
          secureTextEntry
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#3EC6FF',
            padding: 10,
            alignItems: 'center',
            marginTop: 20,
          }}
          onPress={() => uploadImage(photo.uri)}>
          <Text style={{color: 'white'}}>REGISTER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Register;
