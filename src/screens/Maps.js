import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1Ijoicml6YWxsbCIsImEiOiJja2c3Z3NxOHAwNzRuMnhvemRzNDdobnJzIn0.Yh42zWQK2ni4iFGFoWpHhw',
);
const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

const Maps = ({navigation}) => {
  const markers = [];
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };
    getLocation();
    setMarkers();
    console.log(markers);
  }, []);

  const setMarkers = () => {
    for (let i = 0; i < coordinates.length; i++) {
      markers.push(
        <MapboxGL.PointAnnotation
          key={i.toString()}
          id={i.toString()}
          coordinate={coordinates[i]}>
          <MapboxGL.Callout
            title={`Longitude : ${coordinates[i][0]}\nLatitude: ${coordinates[i][1]}`}
          />
        </MapboxGL.PointAnnotation>,
      );
    }
  };

  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView style={{flex: 1}} zoomEnabled={true} zoomLevel={5}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {markers}
      </MapboxGL.MapView>
    </View>
  );
};

export default Maps;

const styles = StyleSheet.create({});
