import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, processColor} from 'react-native';
import {BarChart} from 'react-native-charts-wrapper';

const Chart = () => {
  const [isUpdate, setisUpdate] = useState(false);
  const [data, setData] = useState([
    {y: [100, 40], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [80, 60], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [40, 90], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [78, 45], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [67, 87], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [98, 32], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [150, 90], marker: ['React Native Dasar', 'React Native Lanjutan']},
  ]);

  const dataBaru = [];

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: 'SQUARE',
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });
  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: data,
          label: '',
          config: {
            colors: [processColor('#3EC6FF'), processColor('#088dc4')],
            stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });
  const [xAxis, setXAxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Oct',
      'Nov',
      'Des',
    ],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });
  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  useEffect(() => {
    for (let i = 0; i < data.length; i++) {
      dataBaru.push({
        y: [data[i].y[0], data[i].y[1]],
        marker: [
          `${data[i].marker[0]}\n${data[i].y[0]}`,
          `${data[i].marker[1]}\n${data[i].y[1]}`,
        ],
      });
    }
    setData(dataBaru);
    setChart((chart) => ({
      data: {
        dataSets: [{...chart.data.dataSets[0], values: data}],
      },
    }));
    setisUpdate(true);
  }, [isUpdate]);

  return (
    <View style={{flex: 1}}>
      <BarChart
        style={{flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        legend={legend}
        chartDescription={{text: ''}}
        doubleTapToZoomEnabled={false}
        pinchZoom={false}
        marker={{
          enabled: true,
          textSize: 14,
        }}
      />
    </View>
  );
};

export default Chart;

const styles = StyleSheet.create({});
