import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Chat = ({route, navigation}) => {
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});
  useEffect(() => {
    const user = auth().currentUser;
    setUser(user);
    getData();
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getData = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((prevMessage) => GiftedChat.append(prevMessage, value));
      });
  };

  const onSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };
  return (
    <View style={{flex: 1}}>
      <GiftedChat
        messages={messages}
        onSend={(messages) => onSend(messages)}
        user={{
          _id: user.uid,
          avatar:
            'https://ui-avatars.com/api/?name=Rizal&background=0D8ABC&color=fff',
          name: user.email,
        }}
      />
    </View>
  );
};

export default Chat;
