import React, {useEffect, useState} from 'react';
import {View, Text, Image, TouchableOpacity, StatusBar} from 'react-native';
import api from '../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);
  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        if (token != null) {
          return getVenue(token);
        }
      } catch (error) {
        console.log(error);
      }
    };
    getToken();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    if (await GoogleSignin.isSignedIn()) {
      const userInfo = await GoogleSignin.signInSilently();
      console.log(userInfo);
      if (userInfo != null) {
        setUserInfo(userInfo);
      }
    }
  };

  const getVenue = (token) => {
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {Authorization: 'Bearer' + token},
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.log(err));
  };

  const onLogoutPress = async () => {
    try {
      if (await GoogleSignin.isSignedIn()) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('isLogin');
      navigation.replace('Auth');
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View
        style={{
          height: '35%',
          backgroundColor: '#3EC6FF',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={
            userInfo == null
              ? require('../../assets/image/profile_pic.jpg')
              : {uri: userInfo.user.photo}
          }
          style={{height: 100, width: 100, borderRadius: 50}}
        />
        <Text style={{color: 'white', fontSize: 18, fontWeight: '500'}}>
          {userInfo == null ? 'Rizal Endar Wibowo' : userInfo.user.name}
        </Text>
      </View>

      <View
        style={{
          elevation: 3,
          backgroundColor: 'white',
          borderRadius: 10,
          padding: 10,
          marginHorizontal: 20,
          marginTop: -30,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
          }}>
          <Text>Tanggal Lahir</Text>
          <Text>18 Januari 1998</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
          }}>
          <Text>Jenis Kelamin</Text>
          <Text>Laki-laki</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
          }}>
          <Text>Hobi</Text>
          <Text>Ngoding</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
          }}>
          <Text>No. Telp</Text>
          <Text>082251713937</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
          }}>
          <Text>Email</Text>
          <Text>
            {userInfo == null ? 'rizalendar.re@gmail.com' : userInfo.user.email}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            backgroundColor: '#3EC6FF',
            padding: 10,
            alignItems: 'center',
            marginTop: 20,
          }}
          onPress={() => onLogoutPress()}>
          <Text style={{color: 'white'}}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Profile;
